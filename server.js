const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.use(express.static('public'));



io.on('connection', (socket) => {
    socket.on('checkPassword', ({ username, room, password }) => {
      if (password === 'free') {
        socket.join(room);
        socket.emit('passwordFeedback', { success: true });
      } else {
        socket.emit('passwordFeedback', { success: false });
      }
    });
  });





  const messages = [];

  io.on('connection', (socket) => {
    socket.emit('pastMessages', messages);
  
    socket.on('chatMessage', (message) => {
      messages.push(message);
      io.emit('chatMessage', message);
    });
  });
  


  
  http.listen(3000, () => {
    console.log('listening on *:3000');
  });
  