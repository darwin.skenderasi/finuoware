const express = require('express');
const http = require('http');
const socketIo = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

const PORT = 3000;
const PRIVATE_CHAT_PASSWORD = 'secret';

app.use(express.static('public'));

io.on('connection', (socket) => {
    let username;

    socket.on('setUsername', (data) => {
        username = data;
        socket.emit('usernameSet', username);
    });

    socket.on('passwordCheck', (password) => {
        if (password === 'free') {
            socket.join('public');
            io.to('public').emit('message', `${username} joined the chat.`);
        } else if (password === PRIVATE_CHAT_PASSWORD) {
            socket.join('private');
            io.to('private').emit('message', `${username} joined the private chat.`);
        } else {
            socket.emit('joinError', 'Incorrect password.');
        }
    });

    socket.on('sendMessage', (message) => {
        let room = password === 'free' ? 'public' : 'private';
        if (room) {
            io.to(room).emit('message', `${username}: ${message}`);
        }
    });
});

server.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
